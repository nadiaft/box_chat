use box_chat;
CREATE TABLE IF NOT EXISTS users( 
   user_id INT AUTO_INCREMENT PRIMARY KEY,
   first_name VARCHAR(40) NOT NULL,
   last_name VARCHAR(40)NOT NULL,
   pseudo VARCHAR(40) UNIQUE,
   password VARCHAR(40)NOT NULL, 
   email VARCHAR(40)NOT NULL,
   registration_date datetime NOT NULL,
   CONSTRAINT uc_pseudo_email UNIQUE (pseudo,email),
   users_STATUS boolean NOT NULL DEFAULT true 
  
);
CREATE TABLE IF NOT EXISTS messages(
   message_id INT AUTO_INCREMENT PRIMARY KEY, 
   message VARCHAR(255) NOT NULL,
   send_date datetime NOT NULL,
   user_id INT NOT NULL,
   FOREIGN KEY(user_id) REFERENCES users(user_id)
);

